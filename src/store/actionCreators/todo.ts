import axios from 'axios';
import { Dispatch } from 'react';
import { ITodo, TodoActions, TodoActionTypes } from '../../types/todo';

export const fetchTodos = (page = 1, limit = 10) => {
  return async (dispatch: Dispatch<TodoActions>) => {
    try {
      dispatch({ type: TodoActionTypes.FETCH_TODOS });

      const todos = await axios.get<ITodo[]>(
        'https://jsonplaceholder.typicode.com/todos',
        {
          params: {
            _page: page,
            _limit: limit,
          },
        }
      );

      dispatch({
        type: TodoActionTypes.FETCH_TODOS_SUCCESS,
        payload: todos.data,
      });
    } catch (error) {
      dispatch({
        type: TodoActionTypes.FETCH_TODOS_ERROR,
        payload: `Произошла ошибка при загрузке списка дел: ${error}`,
      });
    }
  };
};

export const setTodoPage = (page: number): TodoActions => {
  return { type: TodoActionTypes.SET_TODO_PAGE, payload: page };
};
