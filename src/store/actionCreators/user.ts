import axios from 'axios';
import { Dispatch } from 'react';
import { IUser, UserActions, UserActionTypes } from '../../types/user';

export const fetchUsers = () => {
  return async (dispatch: Dispatch<UserActions>) => {
    try {
      dispatch({ type: UserActionTypes.FETCH_USERS });

      const users = await axios.get<IUser[]>(
        'https://jsonplaceholder.typicode.com/users'
      );

      setTimeout(() => {
        dispatch({
          type: UserActionTypes.FETCH_USERS_SUCCESS,
          payload: users.data,
        });
      }, 1000);
    } catch (error) {
      dispatch({
        type: UserActionTypes.FETCH_USERS_ERROR,
        payload: `Произошла ошибка при загрузке пользователей: ${error}`,
      });
    }
  };
};
