import { combineReducers } from 'redux';
import { todosReducer } from './todosReducer';
import { usersReducer } from './usersReducer';

export const rootReducer = combineReducers({
  user: usersReducer,
  todo: todosReducer,
});

export type RootReducer = ReturnType<typeof rootReducer>;
