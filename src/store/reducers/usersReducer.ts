import { UserActions, UserActionTypes, UserState } from '../../types/user';

const initialState: UserState = {
  users: [],
  isLoading: false,
  errors: null,
};

export const usersReducer = (
  state = initialState,
  action: UserActions
): UserState => {
  switch (action.type) {
    case UserActionTypes.FETCH_USERS:
      return { isLoading: true, errors: null, users: [] };

    case UserActionTypes.FETCH_USERS_SUCCESS:
      return { isLoading: false, errors: null, users: action.payload };

    case UserActionTypes.FETCH_USERS_ERROR:
      return { isLoading: false, errors: action.payload, users: [] };

    default:
      return state;
  }
};
