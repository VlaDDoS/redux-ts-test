export enum UserActionTypes {
  FETCH_USERS = 'FETCH_USERS',
  FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS',
  FETCH_USERS_ERROR = 'FETCH_USERS_ERROR',
}

interface FetchUsersAction {
  type: UserActionTypes.FETCH_USERS;
}

interface FetchUsersSuccessAction {
  type: UserActionTypes.FETCH_USERS_SUCCESS;
  payload: IUser[];
}

interface FetchUsersErrorAction {
  type: UserActionTypes.FETCH_USERS_ERROR;
  payload: string;
}

export type UserActions =
  | FetchUsersAction
  | FetchUsersSuccessAction
  | FetchUsersErrorAction;

interface IAddress {
  street: string;
  city: string;
}

export interface IUser {
  id: number;
  name: string;
  address: IAddress;
}

export interface UserState {
  users: IUser[];
  isLoading: boolean;
  errors: string | null;
}
