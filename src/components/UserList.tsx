import React, { FC, useEffect } from 'react';
import { useActions } from '../hooks/useActions';
import { useTypedSelect } from '../hooks/useTypedSelect';

export const UserList: FC = () => {
  const { isLoading, errors, users } = useTypedSelect((state) => state.user);
  const { fetchUsers } = useActions();

  useEffect(() => {
    fetchUsers();
  }, []);

  if (isLoading) {
    return <h1>Идёт загрузка...</h1>;
  }

  if (errors) {
    return <h1>{errors}</h1>;
  }

  return (
    <div>
      {users.map((user) => (
        <div key={user.id}>
          <p>
            {user.id}.<b>{user.name}</b> живёт в городе{' '}
            <b>{user.address.city}</b> на улице <b>{user.address.street}</b>
          </p>
        </div>
      ))}
    </div>
  );
};
