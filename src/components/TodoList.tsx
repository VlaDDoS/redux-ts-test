import React, { FC, ReactElement, useEffect } from 'react';
import { useActions } from '../hooks/useActions';
import { useTypedSelect } from '../hooks/useTypedSelect';

export const TodoList: FC = () => {
  const { isLoading, todos, errors, limit, page } = useTypedSelect(
    (state) => state.todo
  );
  const { fetchTodos, setTodoPage } = useActions();
  const pages = [1, 2, 3, 4, 5];

  useEffect(() => {
    fetchTodos(page, limit);
  }, [page]);

  if (isLoading) {
    return <h1>Идёт загрузка...</h1>;
  }

  if (errors) {
    return <h1>{errors}</h1>;
  }

  const isCompleted = (completed: boolean): ReactElement<HTMLSpanElement> => {
    if (completed) {
      return (
        <span
          style={{
            backgroundColor: 'lime',
            color: 'white',
            padding: '5px',
          }}
        >
          Выполнено
        </span>
      );
    } else {
      return (
        <span
          style={{
            backgroundColor: 'red',
            color: 'white',
            padding: '5px',
          }}
        >
          Не выполнено
        </span>
      );
    }
  };

  return (
    <div>
      {todos.map((todo) => (
        <p key={todo.id}>
          {todo.id}. {todo.title}
          {isCompleted(todo.completed)}
        </p>
      ))}
      <div style={{ display: 'flex' }}>
        {pages.map((p, idx) => (
          <div
            key={idx}
            onClick={() => setTodoPage(p)}
            style={{
              outline: p === page ? '2px solid green' : '1px solid gray',
              padding: 10,
            }}
          >
            {p}
          </div>
        ))}
      </div>
    </div>
  );
};
